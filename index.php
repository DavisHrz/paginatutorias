<?php
session_start();
if( empty($_SESSION["title"]) ){
  $_SESSION["title"] = "Login";
}
require_once("control/control_title.php");
require_once("modelo/CUsuario.php");

$user = new CUsuario();

?>
<!DOCTYPE html>
<html lang="es" dir="ltr">
  <head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="css/login.css">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
    <title><?php echo $_SESSION["title"]; ?></title>
  </head>
  <body>
    <?php
        require_once("vista/vista_Menu.php");
        require_once("control/control_Sesion.php");
        require_once("control/control_TiempoSesion.php");
    ?>
  </body>
</html>