<?php
class CConecta{
   var $host;
   var $user;
   var $pw;
   var $nombreBD;

   function CConecta(){
     $this->host="localhost";
     $this->user="root";
     $this->pw="";
     $this->nombreBD="tutorias";
   }

   function conexion(){
      if($db= mysqli_connect($this->host, $this->user, $this->pw)){
        if(mysqli_select_db($db, $this->nombreBD)){
            return $db;
        }
        else{
          die("Base de datos inexistente");
        }
      }
      else{
        die("No hay conexion");
      }
    }
  }
?>