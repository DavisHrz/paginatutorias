<?php

// Aqui recibira los datos del formulario del login, en caso de que la contraseña este equivocada
// no se guardara nada y se redirigira al index
if( !empty($_GET["log"]) && isset($_GET["log"]) ){
    if( !empty($_POST["USUARIO"]) && isset($_POST["USUARIO"]) 
     && !empty($_POST["PASSWORD"]) && isset($_POST["PASSWORD"]) ){
         $user->USUARIO = $_POST["USUARIO"];
         $user->PASSWORD = $_POST["PASSWORD"];
         if( $user->login() ){
            $_SESSION["ID"] = $user->ID;
            $_SESSION["USER"] = $user->USUARIO;
            $_SESSION["PERFIL"] = $user->PERFIL;
            $_SESSION["TIME"] = $_SERVER['REQUEST_TIME'];
            if( empty($_POST["RECORDARME"]) ){
                //Despues de n segundos que el usuario este ausente, se cerrara sesion
                $_SESSION["TIMEOUT"] = 5;
            }else{
                //Nunca se acabara la sesion del usuario
                $_SESSION["TIMEOUT"] = 0;
            }
         }
    }
    header('Location: index.php');
}

// Si no hay ninguna id guardada(por lo tanto no hay ninguna session iniciada)
if( (empty($_SESSION["ID"])) ){

    if( !empty($_GET["pag"]) && isset($_GET["pag"]) ){
        switch( $_GET["pag"] ){
            case 1:
                include_once("vista/vista_InicioSesion.php");
            break;
            case 2:
                include_once("vista/vista_ContraseñaOlvidada.php");
            break;
            default:
                include_once("vista/vista_InicioSesion.php");
        }
    }else{
        include_once("vista/vista_InicioSesion.php");
    }
    //En caso de haber una id guardada (hay una sesion iniciada) ya nos saltaremos el login
}else{

    include_once("control/control_Inicio.php");
    
}
?>